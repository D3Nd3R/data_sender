#include "data_sender.h"

#include <QDebug>
#include <QAuthenticator>
#include <QByteArray>
#include <QNetworkRequest>
#include <QSslConfiguration>
#include <QVariant>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QDir>
#include <QEventLoop>
#include <QList>
#include <QPair>

#include <exception>

DataSender::DataSender()
{
    mpNetManager = Q_NULLPTR;
    mpNetManager = new QNetworkAccessManager();
    if (mpNetManager == nullptr)
        qDebug() << "can't create QNetworkAccessManager";
    setConnects();
}

DataSender::DataSender(const QString &user, const QString &pass,
                                 const QString &host, const QString &request) :
    mUser(user), mPass(pass),mHost(host) ,mRequest(request)
{
    mpNetManager = Q_NULLPTR;
    mpNetManager = new QNetworkAccessManager();
    if (mpNetManager == nullptr)
        qDebug() << "can't create QNetworkAccessManager";
    setConnects();
}

DataSender::~DataSender()
{
    if (mpNetManager)
        delete mpNetManager;
}

void DataSender::setUser(const QString &user)
{
    mUser = user;
}

void DataSender::setPassword(const QString &pass)
{
    mPass = pass;
}

QString DataSender::getUser() const
{
    return mUser;
}

QString DataSender::getPassword() const
{
    return mPass;
}

void DataSender::uploadData(const QString &camId,
                                   const QByteArray &data)
{
#ifdef QT_DEBUG
    qDebug() <<"-------------------uploadData---------------";
#endif

    mCamId = camId;
    QUrl url(mHost);
    url.setUserName(mUser);
    url.setPassword(mPass);

    QNetworkRequest req = QNetworkRequest(url);
    req.setHeader(QNetworkRequest::ContentTypeHeader,
                  "application/json;encoding=utf-8");

   QSslConfiguration conf = req.sslConfiguration();
    conf.setPeerVerifyMode(QSslSocket::VerifyNone);
    req.setSslConfiguration(conf);


    connect(mpNetManager->post(req, data), SIGNAL(error(QNetworkReply::NetworkError)),
            this, SLOT(onReplyError(QNetworkReply::NetworkError)));
    connect(mpNetManager, &QNetworkAccessManager::finished,
            this, &DataSender::onFinished);
}

void DataSender::shutDown()
{
    if (mpNetManager)
        delete mpNetManager;

    emit emitShutDown();
}

void DataSender::setConnects()
{
    connect(mpNetManager, &QNetworkAccessManager::authenticationRequired,
            this, &DataSender::onAuthenticationRequired);
}

void DataSender::onAuthenticationRequired(QNetworkReply *,
                                               QAuthenticator *authenticator)
{
    authenticator->setUser(mUser);
    authenticator->setPassword(mPass);
}

void DataSender::sslErrors(QNetworkReply *, const QList<QSslError> &errors)
{
    QString errorString;
    foreach (const QSslError &error, errors) {
        if (!errorString.isEmpty())
            errorString += '\n';
        errorString += error.errorString();
    }
    qDebug() << errorString;
}

void DataSender::onReplyError(QNetworkReply::NetworkError code)
{
    qDebug() << code;
}

void DataSender::onFinished(QNetworkReply *reply)
{
    QVariant statusCode = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute);
    disconnect(mpNetManager,&QNetworkAccessManager::finished,
               this,&DataSender::onFinished);
#ifdef QT_DEBUG
        qDebug() << reply->attribute(
                        QNetworkRequest::HttpReasonPhraseAttribute).toString();
        auto headerPairs =  reply->rawHeaderPairs();
       foreach (auto& headerPair, headerPairs) {
            qDebug() << headerPair.first << "   " << headerPair.second;
        }
#endif
    if (statusCode.toInt() == 200)
    {
        auto data = reply->readAll();

        qDebug() << "onFinished";
    }
    else
    {
        qDebug() << "status code: " << statusCode.toString();
        qDebug() << reply->attribute(
                        QNetworkRequest::HttpReasonPhraseAttribute).toString();
    }
#ifdef QT_DEBUG
    qDebug() << "//===========================================================//";
#endif
    reply->deleteLater();
    emit done();
}

void DataSender::onDownloadFile(const QString &camId, const QByteArray &data)
{
    this->uploadData(camId, data);
}

