#include "jsonresponsemaker.h"

#include <exception>

JsonResponseMaker::JsonResponseMaker()
{
    mCoordsInRecord = 0;
}

void JsonResponseMaker::setType(const JsonResponseMaker::RECORD_TYPE type)
{
    mRecordType = type;
}

void JsonResponseMaker::newReport(const QString &imageName,const QString & camId,
                                  RECORD_TYPE recordType)
{
    mpReportObject.reset(new QJsonObject());
    mpCurrentRecord.reset(new QJsonObject());
    mpRecords.reset(new QJsonArray());

    mpReportObject->insert("image_name",imageName);
    mpReportObject->insert("cam",camId);
    mpReportObject->insert("coor_type",
                           (int)recordType == 0 ? "clearance" : "poligon");
    mRecordType = recordType;

    mCoordsInRecord = 0;
}

void JsonResponseMaker::newRecord()
{
    if (!mpCurrentRecord->isEmpty())
    {
        *mpRecords << *mpCurrentRecord;
        mpCurrentRecord.reset(new QJsonObject());
    }
    mCoordsInRecord = 0;
}

void JsonResponseMaker::newRecord(const std::vector<cv::Point> &coords)
{
    this->newRecord();

    if (mRecordType == RECORD_TYPE::CLEARANCE && coords.size() != 4)
        throw std::runtime_error("coords in RECORD_TYPE::CLEARANCE should contain 4 vertexs");

    foreach (auto &vertex, coords) {
        if (mRecordType == RECORD_TYPE::CLEARANCE && mCoordsInRecord > 4)
            throw std::runtime_error("coords in RECORD_TYPE::CLEARANCE should contain 4 vertexs");

        this->putCoord(vertex.x, vertex.y);
    }
}

void JsonResponseMaker::putCoord(coorType x, coorType y)
{
    ++mCoordsInRecord;
    mpCurrentRecord->insert(QString("x%1").arg(mCoordsInRecord),QString::number(x));
    mpCurrentRecord->insert(QString("y%1").arg(mCoordsInRecord),QString::number(y));
}

QJsonDocument JsonResponseMaker::getJsonResponse()
{
    if (!mpCurrentRecord->isEmpty())
    {
        if (mRecordType == RECORD_TYPE::POLIGON)
            *mpRecords << *mpCurrentRecord;
        else if (mRecordType == RECORD_TYPE::CLEARANCE && mCoordsInRecord == 4)
            *mpRecords << *mpCurrentRecord;
    }

    mpReportObject->insert("free_pol",*mpRecords);
    mpReportObject->insert("count", mpRecords->size());
    QJsonDocument response;
    response.setObject(*mpReportObject);



    return response;
}

















