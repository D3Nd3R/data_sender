#include <QCoreApplication>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QDebug>
#include <QThreadPool>
#include <QThread>

#include <iostream>
#include <vector>
#include <utility>
#include <functional>
#include <opencv2/core/core.hpp>

#include "jsonresponsemaker.h"
#include "data_sender.h"

#include <data_sender_worker.h>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);


    JsonResponseMaker jsonResp;
    jsonResp.newReport("2017345.jpg","901000001",
                       JsonResponseMaker::RECORD_TYPE::CLEARANCE);

    jsonResp.newRecord();

    jsonResp.putCoord(1,2);
    jsonResp.putCoord(3,4);
    jsonResp.putCoord(5,6);
    jsonResp.putCoord(7,8);


    jsonResp.newRecord();

    jsonResp.putCoord(9,8);
    jsonResp.putCoord(7,6);
    jsonResp.putCoord(5,4);
    jsonResp.putCoord(3,2);

    jsonResp.newReport("111.jpg","901000010",
                       JsonResponseMaker::RECORD_TYPE::CLEARANCE);

    std::vector<cv::Point> vec(4);
    for(size_t i = 0; i <  vec.size(); ++i)
    {
        vec[i].x = i;
        vec[i].y = i*5 + 1;
    }

    jsonResp.newRecord(vec);
    jsonResp.newRecord();

    jsonResp.putCoord(9,8);
    jsonResp.putCoord(7,6);
    jsonResp.putCoord(5,4);
    jsonResp.putCoord(3,2);
    qDebug() << jsonResp.getJsonResponse();
    /*    DataSender *dataSender = new DataSender("Analitic", "GPanaliticserv","https://analitics.getprk.ru/free_pol","");

    dataSender->uploadData("901000003",jsonResp.getJsonResponse().toJson());
    QObject::connect(dataSender, &DataSender::done, [&dataSender, &jsonResp]()
    {
        dataSender->uploadData("901000003",jsonResp.getJsonResponse().toJson());
    });
*/
    //QThreadPool threaPool;

   /* std::function< void(void)> lmb;
    lmb = [&threaPool, &jsonResp, &lmb] ()->void
    {
        DataSenderWorker *worker1 = new
                DataSenderWorker("Analitic", "GPanaliticserv",
                                 "https://analitics.getprk.ru/free_pol","901000003",
                                 jsonResp.getJsonResponse().toJson());
        QObject::connect(worker1, &DataSenderWorker::done, lmb);
        threaPool.start(worker1);
    };
*/

    QThread workThread;
    DataSenderWorker *worker = new
            DataSenderWorker("Analitic", "GPanaliticserv",
                             "https://analitics.getprk.ru/free_pol","901000010",
                             jsonResp.getJsonResponse().toJson());
    worker->moveToThread(&workThread);
    QObject::connect(&workThread,&QThread::started, worker, &DataSenderWorker::run);
    workThread.start();
   // worker->run();
     //QObject::connect(worker, &DataSenderWorker::done,lmb);

    //threaPool.start(worker);

    return a.exec();
}

