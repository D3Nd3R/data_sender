#include "data_sender_worker.h"

#include <QEventLoop>
#include <QNetworkReply>
#include <QAuthenticator>


DataSenderWorker::DataSenderWorker(const QString &user, const QString &pass,
                                   const QString &host, const QString camId,
                                   const QByteArray &data) : mUser(user),
    mPass(pass),mHost(host),mData(data)
{
    mpNetManager = Q_NULLPTR;
    /*mpNetManager = new QNetworkAccessManager();
    if (mpNetManager == nullptr)
        qDebug() << "can't create QNetworkAccessManager";*/
}

DataSenderWorker::~DataSenderWorker()
{
    delete mpNetManager;
}

void DataSenderWorker::run()
{
    mpNetManager = new QNetworkAccessManager();
    if (mpNetManager == nullptr)
        qDebug() << "can't create QNetworkAccessManager";
    this->sendJsonData();
}

void DataSenderWorker::sendJsonData()
{
#ifdef QT_DEBUG
    qDebug() <<"-------------------uploadData---------------";
#endif
    QEventLoop *pLocalEventLoop = new QEventLoop();

    QUrl url(mHost);
    url.setUserName(mUser);
    url.setPassword(mPass);

    QNetworkRequest req = QNetworkRequest(url);
    req.setHeader(QNetworkRequest::ContentTypeHeader,
                  "application/json;encoding=utf-8");

    QSslConfiguration conf = req.sslConfiguration();
    conf.setPeerVerifyMode(QSslSocket::VerifyNone);
    req.setSslConfiguration(conf);


    connect(mpNetManager->post(req, mData), SIGNAL(error(QNetworkReply::NetworkError)),
            this, SLOT(onReplyError(QNetworkReply::NetworkError)));
    connect(mpNetManager, &QNetworkAccessManager::finished,
            this, &DataSenderWorker::onFinished);
    connect(mpNetManager, &QNetworkAccessManager::authenticationRequired,
            this, &DataSenderWorker::onAuthenticationRequired);

    connect(this, &DataSenderWorker::done, [pLocalEventLoop] ()
    {
        pLocalEventLoop->quit();
    });
    pLocalEventLoop->exec();
    delete pLocalEventLoop;
}

void DataSenderWorker::onAuthenticationRequired(QNetworkReply *,
                                                QAuthenticator *authenticator)
{
    authenticator->setUser(mUser);
    authenticator->setPassword(mPass);
}

void DataSenderWorker::sslErrors(QNetworkReply *, const QList<QSslError> &errors)
{
    QString errorString;
    foreach (const QSslError &error, errors) {
        if (!errorString.isEmpty())
            errorString += '\n';
        errorString += error.errorString();
    }
    qDebug() << errorString;
}

void DataSenderWorker::onReplyError(QNetworkReply::NetworkError code)
{
    qDebug() << code;
}

void DataSenderWorker::onFinished(QNetworkReply *reply)
{
    QVariant statusCode =
            reply->attribute(QNetworkRequest::HttpStatusCodeAttribute);

    disconnect(mpNetManager,&QNetworkAccessManager::finished,
               this,&DataSenderWorker::onFinished);

    emit result(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toString(),
                reply->readAll());
#ifdef QT_DEBUG
    {
        qDebug() << reply->attribute(
                        QNetworkRequest::HttpReasonPhraseAttribute).toString();
        auto headerPairs =  reply->rawHeaderPairs();
        foreach (auto& headerPair, headerPairs) {
            qDebug() << headerPair.first << "   " << headerPair.second;
        }
    }
#endif

#ifdef QT_DEBUG
    qDebug() << "//===========================================================//";
#endif
    reply->deleteLater();
    emit done();
}

