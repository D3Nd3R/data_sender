#ifndef JSONRESPONSEMAKER_H
#define JSONRESPONSEMAKER_H

#include <QScopedPointer>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

#include <vector>

#include <opencv2/core/core.hpp>

using coorType = int;

class JsonResponseMaker
{
public:

    enum class RECORD_TYPE
    {
        CLEARANCE = 0,
        POLIGON
    };

    JsonResponseMaker();

    void setType(const RECORD_TYPE type);
    void newReport(const QString  & imageName,
                   const QString &camId, RECORD_TYPE recordType);
    void newRecord();
    void newRecord(const std::vector<cv::Point> &coords);
    void putCoord(coorType x, coorType y);

    QJsonDocument getJsonResponse();

private:
    QScopedPointer<QJsonObject> mpReportObject;
    QScopedPointer<QJsonArray> mpRecords;
    QScopedPointer<QJsonObject> mpCurrentRecord;
    RECORD_TYPE mRecordType;

    unsigned int mCoordsInRecord;

};

#endif // JSONRESPONSEMAKER_H
