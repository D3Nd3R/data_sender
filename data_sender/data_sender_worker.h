#ifndef DATASENDERWORKER_H
#define DATASENDERWORKER_H

#include <QObject>
#include <QRunnable>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QByteArray>
#include <QString>

class DataSenderWorker : public QObject, public QRunnable
{
    Q_OBJECT

public:
    explicit DataSenderWorker(QObject *parent = 0) = delete;
    DataSenderWorker(const QString &user, const QString &pass,
                     const QString &host, const QString camId,
                     const QByteArray &data);
    ~DataSenderWorker();
    void run() override;

signals:
    void done();
    void result(const QString &retCode, const QString &msg);

private:
    void sendJsonData();

public slots:
    void onAuthenticationRequired(QNetworkReply*,
                                  QAuthenticator *authenticator);
    void sslErrors(QNetworkReply*,const QList<QSslError> &errors);
    void onReplyError(QNetworkReply::NetworkError code);
    void onFinished(QNetworkReply *reply);



private:
    QString mUser;
    QString mPass;
    QString mHost;
    QString mCamId;
    QByteArray mData;

    QNetworkAccessManager *mpNetManager;
};

#endif // DATASENDERWORKER_H
