QT += core network
QT -= gui


INCLUDEPATH += /usr/local/include/opencv2/
LIBS += -L/usr/local/lib -lopencv_core


TARGET = data_sender
CONFIG += console
CONFIG -= app_bundle
CONFIG += c++14

TEMPLATE = app

SOURCES += main.cpp \
    jsonresponsemaker.cpp \
    data_sender.cpp \
    data_sender_worker.cpp

HEADERS += \
    jsonresponsemaker.h \
    data_sender.h \
    data_sender_worker.h

