#ifndef HTTPS_DOWNLOADER_H
#define HTTPS_DOWNLOADER_H

#include <QString>
#include <QNetworkAccessManager>
#include <QObject>
#include <QUrl>
#include <QFile>
#include <QList>
#include <QSslError>
#include <QNetworkReply>
#include <QByteArray>

class DataSender : public QObject
{
    Q_OBJECT
public:
    DataSender();
    DataSender(const QString &user, const QString &pass,
                    const QString &host, const QString &request);
    ~DataSender();

    void setUser(const QString &user);
    void setPassword(const QString &pass);
    void setHost(const QString &host);

    QString getUser() const;
    QString getPassword() const;
    QString getHost() const;

    void uploadData(const QString &camId, const QByteArray &data = "");
    void shutDown();

private:
    void setConnects();

private slots:
    void onAuthenticationRequired(QNetworkReply*,
                                                QAuthenticator *authenticator);
    void sslErrors(QNetworkReply*,const QList<QSslError> &errors);
    void onReplyError(QNetworkReply::NetworkError code);
    void onFinished(QNetworkReply *reply);

    void onDownloadFile(const QString &camId, const QByteArray &data);

signals:
    void emitShutDown();
    void done();

private:
    QString mUser;
    QString mPass;
    QString mHost;
    QString mRequest;
    QString mCamId;

    QNetworkAccessManager *mpNetManager;

    QByteArray mData;
};

#endif // HTTPS_DOWNLOADER_H
